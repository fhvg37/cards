import {Meteor} from "meteor/meteor";
import {Mongo} from "meteor/mongo";
import {SimpleSchema} from "meteor/aldeed:simple-schema";
import {Cardsets} from "./cardsets.js";
import {Leitner, Wozniak} from "./learned.js";
import {Paid} from "./paid.js";
import {check} from "meteor/check";
import {CardEditor} from "./cardEditor";
import {UserPermissions} from "./permissions";

export const Cards = new Mongo.Collection("cards");

function getPreviewCards(cardset_id) {
	let cardset = Cardsets.findOne({_id: cardset_id}, {fields: {_id: 1, owner: 1, cardGroups: 1, kind: 1}});
	let filterQuery = {
		$or: [
			{cardset_id: cardset._id},
			{cardset_id: {$in: cardset.cardGroups}}
		]
	};
	let count = Cards.find(filterQuery).count();
	let cardIdArray = Cards.find(filterQuery, {_id: 1}).map(function (card) {
		return card._id;
	});
	let limit;

	if (count < 10) {
		limit = 2;
	} else {
		limit = 5;
	}

	let j, x, i;
	for (i = cardIdArray.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		x = cardIdArray[i];
		cardIdArray[i] = cardIdArray[j];
		cardIdArray[j] = x;
	}
	while (cardIdArray.length > limit) {
		cardIdArray.pop();
	}
	return Cards.find({_id: {$in: cardIdArray}});
}

if (Meteor.isServer) {
	Meteor.publish("demoCards", function () {
		return Cards.find({
			cardset_id: {
				$in: Cardsets.find({kind: "demo"}).map(function (cardset) {
					return cardset._id;
				})
			}
		});
	});
	Meteor.publish("allCards", function () {
		if (this.userId) {
			if (Roles.userIsInRole(this.userId, [
				'admin',
				'editor'
			])) {
				return Cards.find();
			}
		}
	});
	Meteor.publish("cardsetCards", function (cardset_id) {
		let cardset = Cardsets.findOne({_id: cardset_id}, {fields: {_id: 1, owner: 1, cardGroups: 1, kind: 1}});
		if (this.userId && cardset !== undefined) {
			let paidCardsets = Paid.findOne({user_id: this.userId, cardset_id: cardset._id});
			let filterQuery = {
				$or: [
					{cardset_id: cardset._id},
					{cardset_id: {$in: cardset.cardGroups}}
				]
			};
			if (Roles.userIsInRole(this.userId, [
				'admin',
				'editor',
				'lecturer'
			])) {
				return Cards.find(filterQuery);
			} else if (Roles.userIsInRole(this.userId, 'pro')) {
				if (cardset.owner === this.userId || !cardset.kind.includes("personal")) {
					return Cards.find(filterQuery);
				} else {
					return getPreviewCards(cardset._id);
				}
			} else if (Roles.userIsInRole(this.userId, 'university')) {
				if (cardset.owner === this.userId || cardset.kind.includes("free") || cardset.kind.includes("edu") || paidCardsets !== undefined) {
					return Cards.find(filterQuery);
				} else {
					return getPreviewCards(cardset._id);
				}
			} else {
				if (cardset.owner === this.userId || cardset.kind.includes("free") || paidCardsets !== undefined) {
					return Cards.find(filterQuery);
				} else {
					return getPreviewCards(cardset._id);
				}
			}
		}
	});
}

var CardsSchema = new SimpleSchema({
	subject: {
		type: String,
		optional: true,
		max: CardEditor.getMaxTextLength(1)
	},
	front: {
		type: String,
		optional: true,
		max: CardEditor.getMaxTextLength(2)
	},
	back: {
		type: String,
		optional: true,
		max: CardEditor.getMaxTextLength(2)
	},
	hint: {
		type: String,
		optional: true,
		max: CardEditor.getMaxTextLength(2)
	},
	lecture: {
		type: String,
		optional: true,
		max: CardEditor.getMaxTextLength(2)
	},
	top: {
		type: String,
		optional: true,
		max: CardEditor.getMaxTextLength(2)
	},
	bottom: {
		type: String,
		optional: true,
		max: CardEditor.getMaxTextLength(2)
	},
	cardset_id: {
		type: String
	},
	centerText: {
		type: Boolean,
		optional: true
	},
	centerTextElement: {
		type: [Boolean]
	},
	alignType: {
		type: [Number],
		optional: true
	},
	date: {
		type: Date,
		optional: true
	},
	dateUpdated: {
		type: Date,
		optional: true
	},
	learningGoalLevel: {
		type: Number
	},
	backgroundStyle: {
		type: Number
	},
	learningIndex: {
		type: String,
		optional: true
	},
	learningUnit: {
		type: String,
		optional: true
	},
	originalAuthor: {
		type: String,
		optional: true
	},
	originalAuthorName: {
		type: Object,
		optional: true,
		blackbox: true
	}
});

Cards.attachSchema(CardsSchema);

Meteor.methods({
	addCard: function (cardset_id, subject, content1, content2, content3, content4, content5, content6, centerTextElement, alignType, date, learningGoalLevel, backgroundStyle, learningIndex, learningUnit) {
		check(cardset_id, String);
		check(subject, String);
		check(content1, String);
		check(content2, String);
		check(content3, String);
		check(content4, String);
		check(content5, String);
		check(content6, String);
		check(centerTextElement, [Boolean]);
		check(alignType, [Number]);
		check(date, Date);
		check(learningGoalLevel, Number);
		check(backgroundStyle, Number);
		check(learningIndex, String);
		check(learningUnit, String);
		// Make sure the user is logged in and is authorized
		let cardset = Cardsets.findOne(cardset_id);
		if (UserPermissions.isAdmin() || UserPermissions.isOwner(cardset.owner)) {
			if (cardset.cardType !== 2 || cardset.cardType !== 3 || cardset.cardType !== 5) {
				if (subject === "") {
					throw new Meteor.Error("Missing subject");
				}
			} else {
				if (subject === "" && learningUnit === "") {
					throw new Meteor.Error("Missing subject or reference");
				}
			}
			let card_id = Cards.insert({
				subject: subject.trim(),
				front: content1,
				back: content2,
				hint: content3,
				lecture: content4,
				top: content5,
				bottom: content6,
				cardset_id: cardset_id,
				centerTextElement: centerTextElement,
				alignType: alignType,
				date: date,
				learningGoalLevel: learningGoalLevel,
				backgroundStyle: backgroundStyle,
				learningIndex: learningIndex,
				learningUnit: learningUnit
			}, {trimStrings: false});
			Cardsets.update(cardset_id, {
				$set: {
					quantity: Cards.find({cardset_id: cardset_id}).count(),
					dateUpdated: new Date()
				}
			});
			Meteor.call('updateShuffledCardsetQuantity', cardset_id);
			let cardsets = Cardsets.find({
				$or: [
					{_id: cardset_id},
					{cardGroups: {$in: [cardset_id]}}
				]
			}, {fields: {_id: 1}}).fetch();
			for (let i = 0; i < cardsets.length; i++) {
				Meteor.call('updateLeitnerCardIndex', cardsets[i]._id);
			}
			return card_id;
		} else {
			throw new Meteor.Error("not-authorized");
		}
	},
	copyCard: function (sourceCardset_id, targetCardset_id, card_id) {
		check(sourceCardset_id, String);
		check(targetCardset_id, String);
		check(card_id, String);
		let cardset = Cardsets.findOne(sourceCardset_id);
		if (UserPermissions.isAdmin() || UserPermissions.isOwner(cardset.owner)) {
			let card = Cards.findOne(card_id);
			if (card !== undefined) {
				let content1 = "";
				let content2 = "";
				let content3 = "";
				let content4 = "";
				let content5 = "";
				let content6 = "";
				let learningUnit = "";
				let learningIndex = -1;
				if (card.front !== undefined) {
					content1 = card.front;
				}
				if (card.back !== undefined) {
					content2 = card.back;
				}
				if (card.hint !== undefined) {
					content3 = card.hint;
				}
				if (card.lecture !== undefined) {
					content4 = card.lecture;
				}
				if (card.top !== undefined) {
					content5 = card.top;
				}
				if (card.bottom !== undefined) {
					content6 = card.bottom;
				}
				if (card.learningUnit !== undefined) {
					learningUnit = card.learningUnit;
				}
				Meteor.call("addCard", targetCardset_id, card.subject, content1, content2, content3, content4, content5, content6, "0", card.centerTextElement, card.alignType, card.date, card.learningGoalLevel, card.backgroundStyle, learningIndex, learningUnit);
				return true;
			}
		} else {
			throw new Meteor.Error("not-authorized");
		}
	},
	deleteCard: function (card_id) {
		check(card_id, String);

		let card = Cards.findOne(card_id);
		let cardset = Cardsets.findOne(card.cardset_id);
		if (UserPermissions.isAdmin() || UserPermissions.isOwner(cardset.owner)) {
			var countCards = Cards.find({cardset_id: cardset._id}).count();
			if (countCards <= 5) {
				Cardsets.update(cardset._id, {
					$set: {
						kind: 'personal',
						reviewed: false,
						request: false,
						visible: false
					}
				});
			}

			let result = Cards.remove(card_id);
			Cardsets.update(card.cardset_id, {
				$set: {
					quantity: Cards.find({cardset_id: card.cardset_id}).count(),
					dateUpdated: new Date()
				}
			});

			Meteor.call('updateShuffledCardsetQuantity', cardset._id);

			Leitner.remove({
				card_id: card_id
			});
			Wozniak.remove({
				card_id: card_id
			});
			return result;
		} else {
			throw new Meteor.Error("not-authorized");
		}
	},
	updateCard: function (card_id, subject, content1, content2, content3, content4, content5, content6, centerTextElement,alignType, learningGoalLevel, backgroundStyle, learningIndex, learningUnit) {
		check(card_id, String);
		check(subject, String);
		check(content1, String);
		check(content2, String);
		check(content3, String);
		check(content4, String);
		check(content5, String);
		check(content6, String);
		check(centerTextElement, [Boolean]);
		check(alignType, [Number]);
		check(learningGoalLevel, Number);
		check(backgroundStyle, Number);
		check(learningIndex, String);
		check(learningUnit, String);
		let card = Cards.findOne(card_id);
		let cardset = Cardsets.findOne(card.cardset_id);
		if (UserPermissions.isAdmin() || UserPermissions.isOwner(cardset.owner)) {
			if (cardset.cardType !== 2 || cardset.cardType !== 3 || cardset.cardType !== 5) {
				if (subject === "") {
					throw new Meteor.Error("Missing subject");
				}
			} else {
				if (subject === "" && learningUnit === "") {
					throw new Meteor.Error("Missing subject or reference");
				}
			}
			Cards.update(card_id, {
				$set: {
					subject: subject.trim(),
					front: content1,
					back: content2,
					hint: content3,
					lecture: content4,
					top: content5,
					bottom: content6,
					centerTextElement: centerTextElement,
					alignType: alignType,
					learningGoalLevel: learningGoalLevel,
					backgroundStyle: backgroundStyle,
					learningIndex: learningIndex,
					learningUnit: learningUnit,
					dateUpdated: new Date()
				}
			}, {trimStrings: false});
			Cardsets.update(card.cardset_id, {
				$set: {
					dateUpdated: new Date()
				}
			});
		} else {
			throw new Meteor.Error("not-authorized");
		}
	}
});
